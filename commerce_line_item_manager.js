;(function($) {

  /**
   * Automatically submit the payment redirect form.
   */
  Drupal.behaviors.commerceLineItemManager = {
    attach: function (context, settings) {
      $('div.form-item-commerce-line-items-und-actions-product-sku input', context).focus();
    }
  }
})(jQuery);
